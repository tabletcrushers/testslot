﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.U2D;
using DG.Tweening;

internal enum LifeState
{
    Idle,
    Spinning,
    Bringing,
    Bouncing
}



public class Reel : MonoBehaviour
{
    public int ReelID;

    [SerializeField] private List<string> stripItemAliases;
    [SerializeField] private SpriteAtlas itemAtlas;
    [SerializeField] private int sortingOrder;
    [SerializeField] private float winYPosition = 560f;
    [SerializeField] private float bringingOffset = 10f;
    [SerializeField] private float startBounceOffset = 14f;
    [SerializeField] private float itemHeight = 280f;
    [SerializeField] private GameEvent eventReelStopped;

    private Transform topItemTransform;
    private LifeState lifeState = LifeState.Idle;
    private Transform winItemTransform;
    private bool win;

    public void Spin()
    {
        foreach (Transform itemTransform in transform)
        {
            itemTransform.DOLocalMoveY(itemTransform.localPosition.y + startBounceOffset, .2f)
                .SetEase(Ease.OutSine)
                .OnComplete(() => lifeState = LifeState.Spinning);
        }

        ClearHighliting();
        StartCoroutine(LyfeCycle());
    }

    private void Awake()
    {
        stripItemAliases.Shuffle();

        foreach (string alias in stripItemAliases)
        {
            var item = new GameObject(alias);
            var spriteRenderer = item.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = itemAtlas.GetSprite(alias);
            spriteRenderer.sortingOrder = sortingOrder;
            spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            var y = transform.childCount * itemHeight;
            item.transform.parent = transform;
            item.transform.localPosition = new Vector2(0, y);
            topItemTransform = item.transform;
        }
    }

    public void ApplyResult(string alias, bool win)
    {
        this.win = win;
        topItemTransform.GetComponent<SpriteRenderer>().sprite = itemAtlas.GetSprite(alias);
        winItemTransform = topItemTransform;
        StopReel();
    }

    private void StopReel()
    {
        
        lifeState = LifeState.Bringing;
    }

    private void ClearResult()
    {
        winItemTransform.GetComponent<SpriteRenderer>().sprite = itemAtlas.GetSprite(SymbolAliasProvider.GetRandomAlias());
    }

    private void Move()
    {
        foreach (Transform itemTransform in transform)
        {
            itemTransform.Translate(new Vector2(0, -4200f * Time.deltaTime));
            Circulate(itemTransform);
        }
    }

    private void Circulate(Transform itemTransform)
    {
        if (itemTransform.localPosition.y < 0)
        {
            itemTransform.localPosition = new Vector2(0, itemHeight + topItemTransform.localPosition.y - itemTransform.localPosition.y);
            topItemTransform = itemTransform;

            if (itemTransform == winItemTransform)
            {
                ClearResult();
                winItemTransform = null;
            }
        }
    }

    private void HighlightWin()
    {
        foreach (Transform itemTransform in transform)
        {
            if (itemTransform == winItemTransform)
            {
                itemTransform.DOScale(1.2f, .4f).SetEase(Ease.OutCubic);

                continue;
            }

            itemTransform.GetComponent<SpriteRenderer>().DOColor(new Color(.3f, .3f, .3f, 1), 1f);
        }
    }

    private void ClearHighliting()
    {
        foreach (Transform itemTransform in transform)
        {
            if (itemTransform == winItemTransform)
            {
                itemTransform.DOScale(1f, .4f).SetEase(Ease.OutCubic);

                continue;
            }

            itemTransform.GetComponent<SpriteRenderer>().DOColor(new Color(1f, 1f, 1f, 1), 1f);
        }
    }

    private IEnumerator LyfeCycle()
    {
        while (lifeState == LifeState.Idle)
        {
            yield return null;
        }
        
        while (lifeState == LifeState.Spinning)
        {
            Move();

            yield return null;
        }

        while (lifeState == LifeState.Bringing)
        {
            if (winItemTransform.localPosition.y < winYPosition - bringingOffset) lifeState = LifeState.Bouncing;

            Move();

            yield return null;
        }
        
        var pathLength = winItemTransform.localPosition.y - winYPosition;

        for (var i = 0; i < transform.childCount; i++)
        {
            var itemTransform = transform.GetChild(i);
            var targetY = Mathf.Round((itemTransform.localPosition.y - pathLength) / itemHeight) * itemHeight;
            var tween = itemTransform.DOLocalMoveY(targetY, 1f).SetEase(Ease.OutElastic);

            if (i == transform.childCount - 1)
            {
                tween.OnComplete(() => 
                { 
                    lifeState = LifeState.Idle; 
                    if (win) HighlightWin(); 
                });

                var sequence = DOTween.Sequence();
                sequence﻿﻿﻿﻿﻿.PrependInterval(3).OnComplete(() => { eventReelStopped.Dispatch(); });
            }
        }
    }
}
