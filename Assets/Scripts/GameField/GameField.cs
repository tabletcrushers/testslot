﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameField : MonoBehaviour
{
    [SerializeField] private List<Reel> reels;
    [SerializeField] private GameEvent eventResponseRecieved;
    [SerializeField] private GameEvent eventSpinStarted;
    [SerializeField] private GameEvent eventReelStopped;
    [SerializeField] private GameEvent eventAllReelsStopped;
    [SerializeField] private ParticleSystem particles;

    private int stoppedReelsAmount = 0;

    public void Spin()
    {
        eventSpinStarted.Dispatch();
        StartCoroutine(SpinCoroutine());
    }

    private IEnumerator SpinCoroutine()
    {
        foreach (var reel in reels)
        {
            reel.Spin();
            particles.Stop();

            yield return new WaitForSeconds(.2f);
        }
    }

    private IEnumerator ResponseReceivedCoroutine()
    {
        Repository.repository.TryGetValue("spinResult", out object rawResult);
        var result = (rawResult as SpinResultDTO).result;

        foreach (var reel in reels)
        {
            var alias = result == null ? SymbolAliasProvider.GetRandomAlias() : result[reel.ReelID];
            reel.ApplyResult(alias, result != null);

            yield return new WaitForSeconds(.2f);
        }

        if (result != null)
        {
            var sequence = DOTween.Sequence();
            sequence﻿﻿﻿﻿﻿.PrependInterval(1f).OnComplete(() => { particles.Play(); });
        }
    }

    private void Subscribe()
    {
        eventResponseRecieved.listeners.AddListener(OnResponseReceived);
        eventReelStopped.listeners.AddListener(OnReelStopped);
    }

    private void Unsubscribe()
    {
        eventResponseRecieved.listeners.RemoveListener(OnResponseReceived);
        eventReelStopped.listeners.RemoveListener(OnReelStopped);
    }

    private void OnResponseReceived()
    {
        StartCoroutine(ResponseReceivedCoroutine());
    }

    private void OnReelStopped()
    {
        stoppedReelsAmount++;

        if (stoppedReelsAmount == reels.Count)
        {
            eventAllReelsStopped.Dispatch();
            stoppedReelsAmount = 0;
        }
    }

    private void OnEnable()
    {
        Subscribe();
    }

    private void OnDisable()
    {
        Unsubscribe();
    }
}
