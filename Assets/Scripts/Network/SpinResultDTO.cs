﻿public class SpinResultDTO
{
	public string[] result;
	public int[] reward;

	public SpinResultDTO(string[] result = null, int[] reward = null)
	{
		this.result = result;
		this.reward = reward;
	}
}
