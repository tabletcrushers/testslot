﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkCommunicator : MonoBehaviour
{
    [SerializeField] GameEvent eventResponseRecieved;
    [SerializeField] GameEvent eventSpinStarted;
    [SerializeField] private Text debugTextField;

    private readonly List<SpinResultDTO> resultStubs = new List<SpinResultDTO>();
    private readonly SpinResultDTO losingResult = new SpinResultDTO();
    private readonly WeightedRandomBag<int> itemDrops = new WeightedRandomBag<int>();
    private readonly Dictionary<int, float> probabilityMap = new Dictionary<int, float>();

    private void Awake()
    {
        resultStubs.Add(new SpinResultDTO(new string[3] { SymbolAliasProvider.Bonus, SymbolAliasProvider.Bonus, SymbolAliasProvider.Bonus }, new int[2] { 500, 0 }));
        resultStubs.Add(new SpinResultDTO(new string[3] { SymbolAliasProvider.Boy, SymbolAliasProvider.Boy, SymbolAliasProvider.Boy }, new int[2] { 1000, 0 }));
        resultStubs.Add(new SpinResultDTO(new string[3] { SymbolAliasProvider.Lepricoun, SymbolAliasProvider.Lepricoun, SymbolAliasProvider.Lepricoun }, new int[2] { 1500, 0 }));
        resultStubs.Add(new SpinResultDTO(new string[3] { SymbolAliasProvider.GirlAdult, SymbolAliasProvider.GirlAdult, SymbolAliasProvider.GirlAdult }, new int[2] { 2000, 1 }));
        resultStubs.Add(new SpinResultDTO(new string[3] { SymbolAliasProvider.Girl, SymbolAliasProvider.Girl, SymbolAliasProvider.Girl }, new int[2] { 5000, 5 }));
        resultStubs.Add(new SpinResultDTO(new string[3] { SymbolAliasProvider.Girl, SymbolAliasProvider.Girl, SymbolAliasProvider.Girl }, new int[2] { 0, 10 }));

        itemDrops.AddEntry(0, 10.5f);
        itemDrops.AddEntry(1, 10f);
        itemDrops.AddEntry(2, 25f);
        itemDrops.AddEntry(3, 30f);
        itemDrops.AddEntry(4, 24f);
        itemDrops.AddEntry(5, .5f);

        probabilityMap.Add(0, 10.5f);
        probabilityMap.Add(1, 10f);
        probabilityMap.Add(2, 25f);
        probabilityMap.Add(3, 30f);
        probabilityMap.Add(4, 24f);
        probabilityMap.Add(5, .5f);
    }

    private SpinResultDTO GetResult()
    {
        /*if (UnityEngine.Random.value > .5f)
        {
            return losingResult;
        }*/


        var key = itemDrops.GetRandom();
        var result = resultStubs[key];

        if (result.reward != null)
        {
            probabilityMap.TryGetValue(key, out float probability);
            debugTextField.text = probability.ToString() + "%";
        }

        return resultStubs[key];
    }

    private void OnSpinStarted()
    {
        debugTextField.text = "";
        Repository.repository.Remove("spinResult");
        var result = GetResult();
        Repository.repository.Add("spinResult", result);
        //Debug.Log(result.reward[0] + ", " + result.reward[1]);
        StartCoroutine(Delay(() => eventResponseRecieved.Dispatch(), 4f));
    }

    private void Subscribe()
    {
        eventSpinStarted.listeners.AddListener(OnSpinStarted);
    }

    private void Unsubscribe()
    {
        eventSpinStarted.listeners.RemoveListener(OnSpinStarted);
    }

    private void OnEnable()
    {
        Subscribe();
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    IEnumerator Delay(Action action, float time)
    {
        yield return new WaitForSeconds(time);
        action();
    }
}
