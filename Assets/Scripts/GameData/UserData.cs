﻿using UnityEngine;

[CreateAssetMenu(fileName = "UserData", menuName = "SO/UserData")]
public class UserData : ScriptableObject
{
	public string userName;
	public int coins;
	public int turns;
}
