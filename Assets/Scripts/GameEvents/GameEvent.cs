﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "GameEvent", menuName = "SO/GameEvent")]
public class GameEvent : ScriptableObject
{
    [HideInInspector] public UnityEvent listeners;
    [HideInInspector] public object context;

    public void Dispatch()
    {
        listeners?.Invoke();
    }
}
