﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ActionPanel : MonoBehaviour
{
    [SerializeField] private GameData gameData;
    [SerializeField] private UserData userData;
    [SerializeField] private GameEvent eventAllReelsStopped;
    [SerializeField] private Button spinButton;
    [SerializeField] private Text turnsTextField;
    [SerializeField] private Text coinsTextField;
    [SerializeField] private GameObject chargePopup;

    public void OnSpinClicked()
    {
        spinButton.interactable = false;

        if (userData.turns > 0) UpdateTurns(-1);
        else UpdateCoins(-gameData.spinPrice);
    }

    public void OnOkClicked()
    {
        spinButton.interactable = true;

        UpdateCoins(gameData.spinPrice * 4);
        UpdateTurns(2);

        HideChargeCoinsPopup();
    }

    private void Awake()
    {
        spinButton.interactable = false;

        userData.turns = 0;
        userData.coins = 0;

        turnsTextField.text = "0";
        coinsTextField.text = "0";

        ShowChargeCoinsPopup();
    }

    private void OnEnable()
    {
        Subscribe();
    }

    private void OnDisable()
    {
        Unsubscribe();
    }
    
    private void Subscribe()
    {
        eventAllReelsStopped.listeners.AddListener(OnReelsStopped);
    }

    private void Unsubscribe()
    {
        eventAllReelsStopped.listeners.RemoveListener(OnReelsStopped);
    }

    private void OnReelsStopped()
    {
        CollectRewards();
        
        if (userData.turns == 0 && userData.coins < gameData.spinPrice)
        {
            ShowChargeCoinsPopup();

            return;
        }

        spinButton.interactable = true;
    }

    private void CollectRewards()
    {
        Repository.repository.TryGetValue("spinResult", out object rawResult);
        var result = (SpinResultDTO)rawResult;
        
        if (result == null) return;
        
        if (result.reward != null)
        {
            if (result.reward[0] > 0) UpdateCoins(result.reward[0]);
            if (result.reward[1] > 0) UpdateTurns(result.reward[1]);
        }
    }

    private void UpdateCoins(int coins)
    {
        userData.coins += coins;
        coinsTextField.text = userData.coins.ToString();
    }

    private void UpdateTurns(int turns)
    {
        userData.turns += turns;
        turnsTextField.text = userData.turns.ToString();
    }

    private void ShowChargeCoinsPopup()
    {
        chargePopup.transform.DOScale(1f, .2f).SetEase(Ease.OutCubic);
    }

    private void HideChargeCoinsPopup()
    {
        chargePopup.transform.DOScale(0f, .2f).SetEase(Ease.InCubic);
    }
}
