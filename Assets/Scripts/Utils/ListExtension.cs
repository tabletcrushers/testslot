﻿using System;
using System.Collections.Generic;

public static class ListExtension
{
    private static Random rng = new Random();
    public static void Shuffle(this IList<string> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            string value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}