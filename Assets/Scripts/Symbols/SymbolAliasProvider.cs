﻿using UnityEngine;

public class SymbolAliasProvider
{
	public static string Bonus { get => "bonus"; }
	public static string Boy { get => "boy"; }
	public static string Lepricoun { get => "lepricoun"; }
	public static string GirlAdult { get => "girl_adult"; }
	public static string Girl { get => "girl"; }

	private static readonly string[] aliases = new string[5] { Bonus, Boy, Girl, GirlAdult, Lepricoun };

	public static string GetRandomAlias()
	{
		return aliases[Random.Range(0, aliases.Length)];
	}
}
